const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ratingSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    featureRatings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Feature'
    },
    {
        type: Number
    }]
},{
    timestamps: true
});

var Ratings = mongoose.model('Rating', ratingSchema);

module.exports = Ratings;