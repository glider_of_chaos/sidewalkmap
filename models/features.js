const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const featureSchema = new Schema({
    type: {
        type: String,
        default: 'Feature',
    },
    properties: {
        footway: {
            type: String,
            default: 'sidewalk'
        },
        highway: {
            type: String,
            default: 'footway'
        },
        surface: {
            type: String,
            default: ''
        },
        rating: {
            type: Number,
            min: 0,
            max: 5
        }
    },
    geometry: {
        type: {
            type: String,
            default: 'LineString'
        },
        coordinates: {
            type: [[Number]],
            required: true
        }
    }
},{
    timestamps: true
});

var Features = mongoose.model('Feature', featureSchema);

module.exports = Features;