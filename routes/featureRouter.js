const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Features = require('../models/features');

const featureRouter = express.Router();

featureRouter.use(bodyParser.json());

featureRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Features.find({})
    .then((features) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(features);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, (req,res,next) => {//authenticate.verifyUser, authenticate.verifyAdmin, 
    Features.create(req.body)
    .then((feature) => {
        console.log('Feature created ');
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(feature);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.cors, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /features');
})
.delete(cors.cors, (req, res, next) => {
    res.statusCode = 403;
    res.end('DELETE operation not supported on /features');
});

featureRouter.route('/:featureId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Features.findById(req.params.featureId)
    .then((feature) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(feature);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /features/' + req.params.featureId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    Features.findByIdAndUpdate(req.params.featureId, {
        $set: req.body
    }, { new: true })
    .then((feature) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(feature);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    Features.findByIdAndRemove(req.params.featureId)
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

featureRouter.route('/:featureId')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    Features.findById(req.params.featureId)
    .then((feature) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(feature);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    res.statusCode = 403;
    res.end('POST operation not supported on /features/' + req.params.featureId);
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    Features.findByIdAndUpdate(req.params.featureId, {
        $set: req.body
    }, { new: true })
    .then((feature) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(feature);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    Features.findByIdAndRemove(req.params.featureId)
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = featureRouter;