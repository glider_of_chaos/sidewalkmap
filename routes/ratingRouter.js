const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Ratings = require('../models/rating');

const ratingRouter = express.Router();

ratingRouter.use(bodyParser.json());

ratingRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Ratings.find({
        user: req.user._id 
    })
    .populate('user')
    .populate('features')
    .then((ratings) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(ratings);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Ratings.findOne({
        user: req.user._id 
    })
    .then((rating) => {
        if (rating != null) {
            for (let feature of req.body) {
                if (!rating.features.includes(feature["_id"])) {
                    rating.features.push(feature["_id"]);
                }
            }
            rating.save()
            .then((rating) => {
                Ratings.findById(rating._id)
                .populate('user')
                .populate('features')
                .then((rating) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(rating);
                })
            }, (err) => next(err));
        }
        else {
            let newDoc = {
                "user": req.user._id,
                "features": []
            };

            for (let feature of req.body) {
                if (!newDoc.es.includes([feature["_id"], feature["rating"]])) {
                    newDoc.features.push(feature["_id", feature["rating"]]);
                }
            }
            Ratings.create(newDoc)
            .then((rating) => {
                Ratings.findById(rating._id)
                .populate('user')
                .populate('features')
                .then((rating) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(rating);
                })
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /ratings');
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Ratings.remove({
        user: req.user._id
    })
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

ratingRouter.route('/:featureId')
.options(cors.corsWithOptions, authenticate.verifyUser, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req, res, next) => {
    res.statusCode = 403;
    res.end('GET operation not supported on /ratings/' + req.params.featureId);
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Ratings.findOne({
        user: req.user._id 
    })
    .then((rating) => {
        if (rating != null) {
            if (!rating.features.includes([req.params.featureId, req.params.rating])) {
                rating.features.push([req.params.featureId, req.params.rating]);
                rating.save()
                .then((rating) => {
                    Ratings.findById(rating._id)
                    .populate('user')
                    .populate('features')
                    .then((rating) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(rating);
                    })
                }, (err) => next(err));
            }
        }
        else {
            req.body.user = req.user._id;
            req.body.features = [[req.params.featureId, req.params.rating]];
            Ratings.create(req.body)
            .then((rating) => {
                Ratings.findById(rating._id)
                .populate('user')
                .populate('features')
                .then((rating) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(rating);
                })
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /ratings/' + req.params.featureId);
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Ratings.findOne({
        user: req.user._id 
    })
    .then((rating) => {
        let existingIndex = rating.features.indexOf(req.params.featureId);
        if (existingIndex === -1) {
            err = new Error('Dish ' + req.params.featureId + ' not found in this user ratings');
            err.status = 404;
            return next(err);
        }
        else {
            rating.features.splice(existingIndex, 1);
            rating.save()
            .then((rating) => {
                Ratings.findById(rating._id)
                .populate('user')
                .populate('features')
                .then((rating) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(rating);
                })
            }, (err) => next(err));
        }
    })
});

module.exports = ratingRouter;